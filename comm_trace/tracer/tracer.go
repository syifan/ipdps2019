package tracer

import (
	"fmt"
	"log"
	"reflect"
	"strings"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
)

type Tracer struct {
	akita.LogHookBase
	sb strings.Builder
}

func NewTracer(logger *log.Logger) *Tracer {
	t := new(Tracer)
	t.Logger = logger
	return t
}

func (t *Tracer) Type() reflect.Type {
	return reflect.TypeOf((akita.Req)(nil))
}

func (t *Tracer) Pos() akita.HookPos {
	return akita.ConnStartSendHookPos
}

func (t *Tracer) Func(
	item interface{},
	domain akita.Hookable,
	info interface{},
) {
	switch req := item.(type) {
	case *mem.ReadReq:
		t.Logger.Printf("R %s -> %s addr: 0x%x, size: %d",
			req.Src().Comp.Name(), req.Dst().Comp.Name(),
			req.Address, req.MemByteSize)
	case *mem.WriteReq:
		fmt.Fprintf(&t.sb, "W %s -> %s addr: 0x%x, data: ",
			req.Src().Comp.Name(), req.Dst().Comp.Name(),
			req.Address)
		for _, b := range req.Data {
			fmt.Fprintf(&t.sb, "%02x ", b)
		}
		t.Logger.Print(t.sb.String())
		t.sb.Reset()

	case *mem.DataReadyRsp:
		fmt.Fprintf(&t.sb, "DR %s -> %s data: ",
			req.Src().Comp.Name(), req.Dst().Comp.Name())
		for _, b := range req.Data {
			fmt.Fprintf(&t.sb, "%02x ", b)
		}
		t.Logger.Print(t.sb.String())
		t.sb.Reset()

	case *mem.DoneRsp:
		t.Logger.Printf("WD %s -> %s",
			req.Src().Comp.Name(), req.Dst().Comp.Name())
	}
}
