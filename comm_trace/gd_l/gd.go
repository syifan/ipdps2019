package main

import (
	"flag"
	"fmt"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/gcn3"
	"gitlab.com/akita/gcn3/driver"
	"gitlab.com/akita/gcn3/insts"
	"gitlab.com/akita/gcn3/kernels"
	"gitlab.com/akita/gcn3/platform"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/noc"
	"gitlab.com/syifan/ipdps2019/comm_trace/expplatforms"
)

type GDKernelArgs struct {
	Model               driver.GPUPtr
	Gradient0           driver.GPUPtr
	Gradient1           driver.GPUPtr
	Gradient2           driver.GPUPtr
	Gradient3           driver.GPUPtr
	NumCopies           uint32
	Padding             uint32
	HiddenGlobalOffsetX int64
	HiddenGlobalOffsetY int64
	HiddenGlobalOffsetZ int64
}

var (
	engine    akita.Engine
	globalMem *mem.IdealMemController
	gpu       *gcn3.GPU
	gpuDriver *driver.Driver
	interConn *noc.FixedBandwidthConnection

	hsaco *insts.HsaCo

	numParam     int
	numCopy      int
	modelData    []float32
	gradientData [][]float32
	gModel       driver.GPUPtr
	gGradient    []driver.GPUPtr
)

var kernelFilePath = flag.String(
	"kernel file path",
	"kernels.hsaco",
	"The path to the kernel hsaco file.",
)
var timing = flag.Bool("timing", false, "Run detailed timing simulation.")
var parallel = flag.Bool("parallel", false, "Run the simulation in parallel.")
var isaDebug = flag.Bool("debug-isa", false, "Generate the ISA debugging file.")
var instTracing = flag.Bool("trace-inst", false, "Generate instruction trace for visualization purposes.")
var verify = flag.Bool("verify", false, "Verify the emulation result.")
var memTracing = flag.Bool("trace-mem", false, "Generate memory trace")

var numParamFlag = flag.Int("x", 4096, "The number of paramaters in the model.")

func main() {
	configure()
	initPlatform()
	loadProgram()
	initMem()
	run()

	if *verify {
		checkResult()
	}
}

func configure() {
	flag.Parse()

	if *parallel {
		platform.UseParallelEngine = true
	}

	if *isaDebug {
		platform.DebugISA = true
	}

	if *instTracing {
		platform.TraceInst = true
	}

	if *memTracing {
		platform.TraceMem = true
	}

	numParam = *numParamFlag
	numCopy = 4

}

func initPlatform() {
	engine, gpuDriver, gpu, globalMem, interConn =
		expplatforms.BuildLogicalGPU()
}

func loadProgram() {
	hsaco = kernels.LoadProgram(*kernelFilePath, "GD")
}

func initMem() {
	gModel = gpuDriver.AllocateMemory(globalMem.Storage, uint64(numParam*4))
	gGradient = make([]driver.GPUPtr, numCopy)
	for i := 0; i < numCopy; i++ {
		gGradient[i] = gpuDriver.AllocateMemory(
			globalMem.Storage, uint64(numParam*4))
	}

	modelData = make([]float32, numParam)
	for i := 0; i < numCopy; i++ {
		modelData[i] = float32(i)
	}

	gradientData = make([][]float32, numCopy)
	for i := 0; i < numCopy; i++ {
		gradientData[i] = make([]float32, numParam)
		for j := 0; j < numParam; j++ {
			gradientData[i][j] = float32(j)
		}
	}

	gpuDriver.MemoryCopyHostToDevice(gModel, modelData, gpu.ToDriver)
	for i := 0; i < numCopy; i++ {
		gpuDriver.MemoryCopyHostToDevice(gGradient[i], gradientData[i],
			gpu.ToDriver)
	}
}

func run() {
	kernArg := GDKernelArgs{
		gModel,
		gGradient[0],
		gGradient[1],
		gGradient[2],
		gGradient[3],
		uint32(numCopy),
		0,
		0, 0, 0,
	}

	gpuDriver.LaunchKernel(hsaco, gpu.ToDriver, globalMem.Storage,
		[3]uint32{uint32(numParam), 1, 1},
		[3]uint16{64, 1, 1},
		&kernArg,
	)
	fmt.Printf("Data over interconnection: %d\n", interConn.TotalDataSent)
}

func (b *Benchmark) Verify() {
}
