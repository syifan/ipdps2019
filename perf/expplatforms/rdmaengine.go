package expplatforms

import (
	"log"
	"reflect"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/mem/cache"
)

type Compressor interface {
	Compress(addr uint64, data []byte) uint32
	CompressCycles(now akita.VTimeInSec, req akita.Req) int
	DecompressCycles(now akita.VTimeInSec, req akita.Req) int
	NoneCount() int
	BDICount() int
	FPCCount() int
	CPACKCount() int
}

// An RDMAEngine is a component that helps one GPU to access the memory on
// another GPU
type RDMAEngine struct {
	*akita.ComponentBase
	ticker *akita.Ticker

	ToOutside *akita.Port
	ToInside  *akita.Port

	engine              akita.Engine
	compressor          Compressor
	compressing         akita.Req
	compressCycleLeft   int
	decompressing       akita.Req
	decompressCycleLeft int
	localModules        cache.LowModuleFinder
	remoteModules       cache.LowModuleFinder
	originalSrc         map[string]*akita.Port

	freq     akita.Freq
	needTick bool
}

func (e *RDMAEngine) Handle(evt akita.Event) error {
	switch evt := evt.(type) {
	case *akita.TickEvent:
		e.tick(evt.Time())
	default:
		log.Panicf("cannot handle event of type %s", reflect.TypeOf(evt))
	}
	return nil
}

func (e *RDMAEngine) tick(now akita.VTimeInSec) {
	e.needTick = false

	e.processReqFromInside(now)
	e.processReqFromOutside(now)
	e.updateCompressor(now)
	e.updateDecompressor(now)

	if e.needTick {
		e.ticker.TickLater(now)
	}
}

func (e *RDMAEngine) updateCompressor(now akita.VTimeInSec) {
	if e.compressCycleLeft > 0 {
		e.compressCycleLeft--
		e.needTick = true
	}

	if e.compressCycleLeft <= 0 && e.compressing != nil {
		switch req := e.compressing.(type) {
		case *mem.WriteReq:
			e.compressWriteReq(req)
			dst := e.remoteModules.Find(req.Address)
			e.sendCompressedReqToOutside(now, req, dst)
		case *mem.DataReadyRsp:
			e.compressDataReady(req)
			e.sendCompressedRspToOutside(now, req)
		default:
			log.Panicf("unknown type %s", reflect.TypeOf(req))
		}
	}
}

func (e *RDMAEngine) updateDecompressor(now akita.VTimeInSec) {
	if e.decompressCycleLeft > 0 {
		e.decompressCycleLeft--
		e.needTick = true
	}

	if e.decompressCycleLeft <= 0 && e.decompressing != nil {
		switch req := e.decompressing.(type) {
		case *mem.WriteReq:
			dst := e.localModules.Find(req.Address)
			e.sendDecompressedReqToInside(now, req, dst)
		case *mem.DataReadyRsp:
			e.sendDecompressedRspToInside(now, req)
		default:
			log.Panicf("unknown type %s", reflect.TypeOf(req))
		}
	}
}

func (e *RDMAEngine) processReqFromInside(now akita.VTimeInSec) {
	req := e.ToInside.Peek()
	if req == nil {
		return
	}

	switch req := req.(type) {
	case *mem.ReadReq:
		dst := e.remoteModules.Find(req.Address)
		e.sendReqToOutside(now, req, dst)
	case *mem.WriteReq, *mem.DataReadyRsp:
		if e.compressing == nil {
			//log.Printf("%.9f %s comp rsp %s (%d)\n", now, e.Name(), req.GetID(), req.ByteSize())
			e.compressing = req
			e.compressCycleLeft = e.compressor.CompressCycles(now, req)
			e.ToInside.Retrieve(now)
			e.needTick = true
		}
	case *mem.DoneRsp:
		e.sendRspToOutside(now, req)
	default:
		log.Panicf("cannot process request of type %s", reflect.TypeOf(req))
	}
}

func (e *RDMAEngine) sendReqToOutside(now akita.VTimeInSec, req akita.Req, dst *akita.Port) {
	originalByteSize := req.ByteSize()

	originalSrc := req.Src()
	req.SetSrc(e.ToOutside)
	req.SetDst(dst)
	req.SetSendTime(now)
	err := e.ToOutside.Send(req)
	if err == nil {
		e.ToInside.Retrieve(now)
		e.originalSrc[req.GetID()] = originalSrc
		e.needTick = true
		//log.Printf("%.9f %s send req %s (%d)\n", now, e.Name(), req.GetID(), req.ByteSize())
	} else {
		//log.Printf("%.9f %s send req stall %s (%d)\n", now, e.Name(), req.GetID(), req.ByteSize())
		req.SetSrc(originalSrc)
		req.SetDst(e.ToInside)
		req.SetByteSize(originalByteSize)
	}
}

func (e *RDMAEngine) sendCompressedReqToOutside(now akita.VTimeInSec, req akita.Req, dst *akita.Port) {
	originalByteSize := req.ByteSize()

	originalSrc := req.Src()
	req.SetSrc(e.ToOutside)
	req.SetDst(dst)
	req.SetSendTime(now)
	err := e.ToOutside.Send(req)
	if err == nil {
		e.originalSrc[req.GetID()] = originalSrc
		e.compressing = nil
		e.needTick = true
		//log.Printf("%.9f %s send req %s (%d)\n", now, e.Name(), req.GetID(), req.ByteSize())
	} else {
		//log.Printf("%.9f %s send req stall %s (%d)\n", now, e.Name(), req.GetID(), req.ByteSize())
		req.SetSrc(originalSrc)
		req.SetDst(e.ToInside)
		req.SetByteSize(originalByteSize)
	}
}

func (e *RDMAEngine) doCompression(req akita.Req) {
	switch req := req.(type) {
	case *mem.DataReadyRsp:
		e.compressDataReady(req)
	case *mem.WriteReq:
		e.compressWriteReq(req)
	default:
		// Do nothing
	}
}

func (e *RDMAEngine) compressDataReady(req *mem.DataReadyRsp) {
	originalSize := len(req.Data)
	compressedSize := originalSize
	if e.compressor != nil {
		compressedSize = int(e.compressor.Compress(0, req.Data) / 8)
	}

	if compressedSize > originalSize {
		compressedSize = originalSize
	}

	//fmt.Printf("compression %d -> %d\n", originalSize, compressedSize)

	req.CompressedByteSize = compressedSize + 1
}

func (e *RDMAEngine) compressWriteReq(req *mem.WriteReq) {
	originalSize := len(req.Data)
	compressedSize := originalSize
	if e.compressor != nil {
		compressedSize = int(e.compressor.Compress(0, req.Data) / 8)
	}

	if compressedSize > originalSize {
		compressedSize = originalSize
	}

	req.CompressedByteSize = len(req.Data) - (originalSize - compressedSize) + 1
}

func (e *RDMAEngine) sendRspToOutside(now akita.VTimeInSec, req mem.MemRsp) {
	originalByteSize := req.ByteSize()

	recoverSrc := req.Src()
	src, found := e.originalSrc[req.GetRespondTo()]
	if !found {
		log.Panic("original src not found")
	}
	req.SetDst(src)
	req.SetSrc(e.ToOutside)
	req.SetSendTime(now)
	err := e.ToOutside.Send(req)
	if err == nil {
		e.ToInside.Retrieve(now)
		delete(e.originalSrc, req.GetRespondTo())
		e.needTick = true
		//log.Printf("%.9f %s send rsp %s (%d)\n", now, e.Name(), req.GetID(), req.ByteSize())
	} else {
		req.SetSrc(recoverSrc)
		req.SetDst(e.ToInside)
		req.SetByteSize(originalByteSize)
	}
}

func (e *RDMAEngine) sendCompressedRspToOutside(now akita.VTimeInSec, req mem.MemRsp) {
	originalByteSize := req.ByteSize()

	recoverSrc := req.Src()
	src, found := e.originalSrc[req.GetRespondTo()]
	if !found {
		log.Panic("original src not found")
	}
	req.SetDst(src)
	req.SetSrc(e.ToOutside)
	req.SetSendTime(now)
	err := e.ToOutside.Send(req)
	if err == nil {
		delete(e.originalSrc, req.GetRespondTo())
		e.compressing = nil
		e.needTick = true
		//log.Printf("%.9f %s send rsp %s (%d)\n", now, e.Name(), req.GetID(), req.ByteSize())
	} else {
		req.SetSrc(recoverSrc)
		req.SetDst(e.ToInside)
		req.SetByteSize(originalByteSize)
	}
}

func (e *RDMAEngine) processReqFromOutside(now akita.VTimeInSec) {
	req := e.ToOutside.Peek()
	if req == nil {
		return
	}

	//log.Printf("%.9f %s recv req %s (%d)\n", now, e.Name(), req.GetID(), req.ByteSize())
	switch req := req.(type) {
	case *mem.ReadReq:
		dst := e.localModules.Find(req.Address)
		e.sendReqToInside(now, req, dst)
	case *mem.WriteReq, *mem.DataReadyRsp:
		if e.decompressing == nil {
			e.decompressing = req
			e.decompressCycleLeft = e.compressor.DecompressCycles(now, req)
			e.ToOutside.Retrieve(now)
			e.needTick = true
		}
	case *mem.DoneRsp:
		e.sendRspToInside(now, req)
	default:
		log.Panicf("cannot process request of type %s", reflect.TypeOf(req))
	}
}

func (e *RDMAEngine) sendReqToInside(now akita.VTimeInSec, req akita.Req, dst *akita.Port) {

	originalSrc := req.Src()
	req.SetSrc(e.ToInside)
	req.SetDst(dst)
	req.SetSendTime(now)
	err := e.ToInside.Send(req)
	if err == nil {
		e.ToOutside.Retrieve(now)
		e.originalSrc[req.GetID()] = originalSrc
		e.needTick = true
	} else {
		req.SetSrc(originalSrc)
		req.SetDst(e.ToOutside)
	}
}

func (e *RDMAEngine) sendDecompressedReqToInside(now akita.VTimeInSec, req akita.Req, dst *akita.Port) {

	originalSrc := req.Src()
	req.SetSrc(e.ToInside)
	req.SetDst(dst)
	req.SetSendTime(now)
	err := e.ToInside.Send(req)
	if err == nil {
		e.originalSrc[req.GetID()] = originalSrc
		e.decompressing = nil
		e.needTick = true
	} else {
		req.SetSrc(originalSrc)
		req.SetDst(e.ToOutside)
	}
}

func (e *RDMAEngine) sendRspToInside(now akita.VTimeInSec, req mem.MemRsp) {
	recoverSrc := req.Src()
	src, found := e.originalSrc[req.GetRespondTo()]
	if !found {
		log.Panic("original src not found")
	}
	req.SetDst(src)
	req.SetSrc(e.ToInside)
	req.SetSendTime(now)
	err := e.ToInside.Send(req)
	if err == nil {
		e.ToOutside.Retrieve(now)
		delete(e.originalSrc, req.GetRespondTo())
		e.needTick = true
	} else {
		req.SetSrc(recoverSrc)
		req.SetDst(e.ToOutside)
	}
}

func (e *RDMAEngine) sendDecompressedRspToInside(now akita.VTimeInSec, req mem.MemRsp) {
	recoverSrc := req.Src()
	src, found := e.originalSrc[req.GetRespondTo()]
	if !found {
		log.Panic("original src not found")
	}
	req.SetDst(src)
	req.SetSrc(e.ToInside)
	req.SetSendTime(now)
	err := e.ToInside.Send(req)
	if err == nil {
		delete(e.originalSrc, req.GetRespondTo())
		e.decompressing = nil
		e.needTick = true
	} else {
		req.SetSrc(recoverSrc)
		req.SetDst(e.ToOutside)
	}
}

func (e *RDMAEngine) NotifyRecv(now akita.VTimeInSec, port *akita.Port) {
	e.ticker.TickLater(now)
}

func (e *RDMAEngine) NotifyPortFree(now akita.VTimeInSec, port *akita.Port) {
	e.ticker.TickLater(now)
}

func (e *RDMAEngine) SetFreq(freq akita.Freq) {
	e.freq = freq
}

func NewEngine(
	name string,
	engine akita.Engine,
	compressor Compressor,
	localModules cache.LowModuleFinder,
	remoteModules cache.LowModuleFinder,
) *RDMAEngine {
	e := new(RDMAEngine)
	e.freq = 1 * akita.GHz
	e.ComponentBase = akita.NewComponentBase(name)
	e.ticker = akita.NewTicker(e, engine, e.freq)

	e.engine = engine
	e.compressor = compressor
	e.localModules = localModules
	e.remoteModules = remoteModules

	e.originalSrc = make(map[string]*akita.Port)

	e.ToInside = akita.NewPort(e)
	e.ToOutside = akita.NewPort(e)

	return e
}
