package expplatforms

import (
	"math/rand"

	"gitlab.com/akita/akita"
)

type TableBasedLowModuleFinder struct {
	ID            int
	DefaultModule *akita.Port
	Modules       map[uint64]*akita.Port
}

func NewTableBasedLowModuleFinder(defaultModule *akita.Port) *TableBasedLowModuleFinder {
	f := new(TableBasedLowModuleFinder)
	f.ID = rand.Int()
	f.DefaultModule = defaultModule
	f.Modules = make(map[uint64]*akita.Port)
	return f
}

func (f *TableBasedLowModuleFinder) Find(address uint64) *akita.Port {
	module, found := f.Modules[address]
	if found {
		return module
	}
	return f.DefaultModule
}

func (f *TableBasedLowModuleFinder) RegisterAddr(addr uint64, module *akita.Port) {
	f.Modules[addr] = module
}

func (f *TableBasedLowModuleFinder) RegisterAddrRange(
	addr, size uint64,
	module *akita.Port,
) {
	for addr <= addr+size {
		f.RegisterAddr(addr, module)
		addr += 64
	}
}
