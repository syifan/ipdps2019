package expplatforms

import (
	"fmt"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/gcn3"
	"gitlab.com/akita/gcn3/driver"
	"gitlab.com/akita/gcn3/gpubuilder"
	"gitlab.com/akita/gcn3/insts"
	"gitlab.com/akita/gcn3/kernels"
	"gitlab.com/akita/gcn3/rdma"
	"gitlab.com/akita/gcn3/timing"
	"gitlab.com/akita/mem"
	"gitlab.com/akita/mem/cache"
	"gitlab.com/akita/noc"
)

func BuildMonolithicGPU() (
	akita.Engine,
	*driver.Driver,
	*gcn3.GPU,
	*mem.IdealMemController,
) {
	engine := akita.NewSerialEngine()
	//engine.AcceptHook(akita.NewEventLogger(log.New(os.Stdout, "", 0)))
	connection := akita.NewDirectConnection(engine)
	gpuDriver := driver.NewDriver(engine)
	gpuName := "GPU1"

	freq := 1000 * akita.MHz

	// Memory
	gpuMem := mem.NewIdealMemController("GlobalMem", engine, 4*mem.GB)
	gpuMem.Freq = freq
	gpuMem.Latency = 310

	// GPU
	gpu := gcn3.NewGPU(gpuName, engine)
	commandProcessor := gcn3.NewCommandProcessor(gpuName+".CommandProcessor", engine)
	commandProcessor.GPUStorage = gpuMem.Storage
	dispatcher := gcn3.NewDispatcher(gpuName+"Dispatcher", engine,
		new(kernels.GridBuilderImpl))
	dispatcher.Freq = freq

	gpu.CommandProcessor = commandProcessor.ToDriver
	commandProcessor.Dispatcher = dispatcher.ToCommandProcessor
	commandProcessor.Driver = gpu.ToCommandProcessor

	cuBuilder := timing.NewBuilder()
	cuBuilder.Engine = engine
	cuBuilder.Freq = freq
	cuBuilder.Decoder = insts.NewDisassembler()
	cuBuilder.ConnToInstMem = connection
	cuBuilder.ConnToScalarMem = connection
	cuBuilder.ConnToVectorMem = connection

	cacheBuilder := new(cache.Builder)
	cacheBuilder.Engine = engine
	dCaches := make([]*cache.WriteAroundCache, 0, 64)
	kCaches := make([]*cache.WriteAroundCache, 0, 16)
	iCaches := make([]*cache.WriteAroundCache, 0, 16)
	l2Caches := make([]*cache.WriteBackCache, 0, 8)

	lowModuleFinderForL2 := new(cache.SingleLowModuleFinder)
	lowModuleFinderForL2.LowModule = gpuMem.ToTop
	cacheBuilder.LowModuleFinder = lowModuleFinderForL2
	lowModuleFinderForL1 := cache.NewInterleavedLowModuleFinder(4096)
	//lowModuleFinderForL1 := new(cache.SingleLowModuleFinder)
	//lowModuleFinderForL1.LowModule = gpuMem.ToTop

	for i := 0; i < 32; i++ {
		l2Cache := cacheBuilder.BuildWriteBackCache(
			fmt.Sprintf("%s.L2_%d", gpuName, i), 16, 256*mem.KB, 4096)
		l2Caches = append(l2Caches, l2Cache)
		commandProcessor.L2Caches = append(commandProcessor.L2Caches, l2Cache)
		l2Cache.DirectoryLatency = 0
		l2Cache.Latency = 110
		l2Cache.SetNumBanks(4096)
		l2Cache.Freq = 1 * akita.GHz
		lowModuleFinderForL1.LowModules = append(
			lowModuleFinderForL1.LowModules, l2Cache.ToTop)
		connection.PlugIn(l2Cache.ToTop)
		connection.PlugIn(l2Cache.ToBottom)
	}

	cacheBuilder.LowModuleFinder = lowModuleFinderForL1
	for i := 0; i < 256; i++ {
		dCache := cacheBuilder.BuildWriteAroundCache(
			fmt.Sprintf("%s.L1D_%02d", gpuName, i), 4, 16*mem.KB, 128)
		dCache.DirectoryLatency = 0
		dCache.Latency = 10
		dCache.SetNumBanks(1)
		connection.PlugIn(dCache.ToTop)
		connection.PlugIn(dCache.ToBottom)
		dCaches = append(dCaches, dCache)
		commandProcessor.ToResetAfterKernel = append(
			commandProcessor.ToResetAfterKernel, dCache,
		)
	}

	for i := 0; i < 64; i++ {
		kCache := cacheBuilder.BuildWriteAroundCache(
			fmt.Sprintf("%s.L1K_%02d", gpuName, i), 4, 16*mem.KB, 16)
		kCache.DirectoryLatency = 0
		kCache.Latency = 1
		kCache.SetNumBanks(1)
		connection.PlugIn(kCache.ToTop)
		connection.PlugIn(kCache.ToBottom)
		kCaches = append(kCaches, kCache)
		commandProcessor.ToResetAfterKernel = append(
			commandProcessor.ToResetAfterKernel, kCache,
		)

		iCache := cacheBuilder.BuildWriteAroundCache(
			fmt.Sprintf("%s.L1I_%02d", gpuName, i), 4, 32*mem.KB, 16)
		iCache.DirectoryLatency = 0
		iCache.Latency = 0
		iCache.SetNumBanks(4)
		connection.PlugIn(iCache.ToTop)
		connection.PlugIn(iCache.ToBottom)
		iCaches = append(iCaches, iCache)
		commandProcessor.ToResetAfterKernel = append(
			commandProcessor.ToResetAfterKernel, iCache,
		)
	}

	for i := 0; i < 256; i++ {
		cuBuilder.CUName = fmt.Sprintf("%s.CU%02d", gpuName, i)
		cuBuilder.InstMem = iCaches[i/4].ToTop
		cuBuilder.ScalarMem = kCaches[i/4].ToTop
		//lowModuleFinderForCU := new(cache.SingleLowModuleFinder)
		//lowModuleFinderForCU.LowModule = dCaches[i].ToTop
		cuBuilder.VectorMemModules = lowModuleFinderForL1
		//cuBuilder.InstMem = gpuMem
		//cuBuilder.ScalarMem = gpuMem
		//cuBuilder.VectorMem = gpuMem
		cu := cuBuilder.Build()
		dispatcher.RegisterCU(cu.ToACE)

		connection.PlugIn(cu.ToACE)
	}

	dmaEngine := gcn3.NewDMAEngine(
		fmt.Sprintf("%s.DMA", gpuName), engine, lowModuleFinderForL2)
	commandProcessor.DMAEngine = dmaEngine.ToCommandProcessor

	connection.PlugIn(gpu.ToCommandProcessor)
	connection.PlugIn(gpu.ToDriver)
	connection.PlugIn(commandProcessor.ToDriver)
	connection.PlugIn(commandProcessor.ToDispatcher)
	connection.PlugIn(dispatcher.ToCommandProcessor)
	connection.PlugIn(dispatcher.ToCUs)
	connection.PlugIn(gpuMem.ToTop)
	connection.PlugIn(dmaEngine.ToCommandProcessor)
	connection.PlugIn(dmaEngine.ToMem)
	connection.PlugIn(gpuDriver.ToGPUs)

	gpu.Driver = gpuDriver.ToGPUs
	gpu.L2CacheFinder = lowModuleFinderForL1

	return engine, gpuDriver, gpu, gpuMem
}

func BuildFourR9NanoPlatform() (
	akita.Engine,
	*driver.Driver,
	[]*gcn3.GPU,
	*mem.IdealMemController,
	*noc.FixedBandwidthConnection,
	[]*rdma.Engine,
	[]*TableBasedLowModuleFinder,
	[]*TableBasedLowModuleFinder,
) {
	engine := akita.NewSerialEngine()
	//engine.AcceptHook(akita.NewEventLogger(log.New(os.Stdout, "", 0)))
	connection := akita.NewDirectConnection(engine)
	gpuDriver := driver.NewDriver(engine)
	storage := mem.NewStorage(16 * mem.GB)
	gpus := make([]*gcn3.GPU, 4)
	globalMems := make([]*mem.IdealMemController, 4)

	gpuBuilder := gpubuilder.NewGPUBuilder(engine)
	gpuBuilder.Driver = gpuDriver
	for i := 0; i < 4; i++ {
		//gpuBuilder.EnableISADebug = true
		gpuBuilder.GPUName = fmt.Sprintf("GPU%d", i)
		gpus[i], globalMems[i] = gpuBuilder.BuildR9Nano()
		globalMems[i].Storage = storage
		//gpuDriver.RegisterStorage(globalMem1.Storage, driver.GPUPtr(0*mem.GB), 4*mem.GB)
		gpus[i].Driver = gpuDriver.ToGPUs
		connection.PlugIn(gpus[i].ToDriver)
	}

	connection.PlugIn(gpuDriver.ToGPUs)

	interConnect := noc.NewFixedBandwidthConnection(16, engine, 1*akita.GHz)
	remoteModuleFinder := make([]*TableBasedLowModuleFinder, 4)
	localModuleFinder := make([]*TableBasedLowModuleFinder, 4)
	rdmaEngines := make([]*rdma.Engine, 0)

	for i := 0; i < 4; i++ {
		remoteModuleFinder[i] = NewTableBasedLowModuleFinder(nil)
		localModuleFinder[i] = NewTableBasedLowModuleFinder(nil)
		localModuleFinder[i].ID = i
		rdmaEngine := configureRDMAEngine(engine, gpus[i], interConnect,
			i, remoteModuleFinder[i], localModuleFinder[i])
		rdmaEngines = append(rdmaEngines, rdmaEngine)
	}

	return engine, gpuDriver,
		gpus,
		globalMems[0],
		interConnect,
		rdmaEngines,
		localModuleFinder,
		remoteModuleFinder
}

func configureRDMAEngine(
	engine akita.Engine,
	gpu *gcn3.GPU,
	interConnect akita.Connection,
	num int,
	remoteModuleFinder *TableBasedLowModuleFinder,
	localModuleFinder *TableBasedLowModuleFinder,
) *rdma.Engine {
	rdmaEngine := rdma.NewEngine(gpu.Name()+".RDMAEngine", engine,
		localModuleFinder, remoteModuleFinder)

	for _, cu := range gpu.CUs {
		cu.(*timing.ComputeUnit).VectorMemModules = localModuleFinder
	}

	for _, l1 := range gpu.CommandProcessor.Comp.(*gcn3.CommandProcessor).ToResetAfterKernel {
		l1.(*cache.WriteAroundCache).LowModuleFinder = localModuleFinder
	}

	gpu.ToCommandProcessor.Conn.PlugIn(rdmaEngine.ToInside)
	interConnect.PlugIn(rdmaEngine.ToOutside)
	return rdmaEngine
}

func BuildLogicalGPU(compressor Compressor, cuFreq akita.Freq, bufCapacity int) (
	akita.Engine,
	*driver.Driver,
	*gcn3.GPU,
	*mem.IdealMemController,
	*noc.FixedBandwidthConnection,
) {
	engine := akita.NewSerialEngine()
	//engine.AcceptHook(akita.NewEventLogger(log.New(os.Stderr, "", 0)))
	connection := akita.NewDirectConnection(engine)
	gpuDriver := driver.NewDriver(engine)
	gpuName := "GPU1"

	interconnect := noc.NewFixedBandwidthConnection(16, engine, 1*akita.GHz)
	interconnect.SrcBufferCapacity = bufCapacity
	interconnect.DstBufferCapacity = bufCapacity

	freq := 1000 * akita.MHz

	// Memory
	gpuMem := mem.NewIdealMemController("GlobalMem", engine, 4*mem.GB)
	gpuMem.Freq = freq
	gpuMem.Latency = 310

	// GPU
	gpu := gcn3.NewGPU(gpuName, engine)
	commandProcessor := gcn3.NewCommandProcessor(gpuName+".CommandProcessor", engine)
	commandProcessor.GPUStorage = gpuMem.Storage
	dispatcher := gcn3.NewDispatcher(gpuName+"Dispatcher", engine,
		new(kernels.GridBuilderImpl))
	dispatcher.Freq = freq

	gpu.DRAMStorage = gpuMem.Storage
	gpu.CommandProcessor = commandProcessor.ToDriver
	commandProcessor.Dispatcher = dispatcher.ToCommandProcessor
	commandProcessor.Driver = gpu.ToCommandProcessor

	cuBuilder := timing.NewBuilder()
	cuBuilder.Engine = engine
	cuBuilder.Freq = cuFreq
	cuBuilder.Decoder = insts.NewDisassembler()
	cuBuilder.ConnToInstMem = connection
	cuBuilder.ConnToScalarMem = connection
	cuBuilder.ConnToVectorMem = connection

	cacheBuilder := new(cache.Builder)
	cacheBuilder.Engine = engine
	dCaches := make([]*cache.WriteAroundCache, 0, 64)
	kCaches := make([]*cache.WriteAroundCache, 0, 16)
	iCaches := make([]*cache.WriteAroundCache, 0, 16)
	l2Caches := make([]*cache.WriteBackCache, 0, 8)

	lowModuleFinderForL2 := new(cache.SingleLowModuleFinder)
	lowModuleFinderForL2.LowModule = gpuMem.ToTop
	cacheBuilder.LowModuleFinder = lowModuleFinderForL2

	lowModuleFinderForL1 := make([]*cache.InterleavedLowModuleFinder, 4)
	for i := 0; i < 4; i++ {
		lowModuleFinderForL1[i] = cache.NewInterleavedLowModuleFinder(4096)
	}
	//lowModuleFinderForL1 := new(cache.SingleLowModuleFinder)
	//lowModuleFinderForL1.LowModule = gpuMem.ToTop

	//file, _ := os.Create("mem.trace")
	//memTracer := trace.NewTracer(file)
	for i := 0; i < 32; i++ {
		l2Cache := cacheBuilder.BuildWriteBackCache(
			fmt.Sprintf("%s.L2_%d", gpuName, i), 16, 256*mem.KB, 4096)
		l2Caches = append(l2Caches, l2Cache)
		commandProcessor.L2Caches = append(commandProcessor.L2Caches, l2Cache)
		l2Cache.DirectoryLatency = 0
		l2Cache.Latency = 110
		l2Cache.SetNumBanks(4096)
		l2Cache.Freq = 1 * akita.GHz
		//l2Cache.AcceptHook(memTracer)
		connection.PlugIn(l2Cache.ToTop)
		connection.PlugIn(l2Cache.ToBottom)
	}

	for i := 0; i < 256; i++ {
		cacheBuilder.LowModuleFinder = lowModuleFinderForL1[i/64]
		dCache := cacheBuilder.BuildWriteAroundCache(
			fmt.Sprintf("%s.L1D_%02d", gpuName, i), 4, 16*mem.KB, 128)
		dCache.DirectoryLatency = 0
		dCache.Latency = 10
		dCache.SetNumBanks(1)
		connection.PlugIn(dCache.ToTop)
		connection.PlugIn(dCache.ToBottom)
		dCaches = append(dCaches, dCache)
		commandProcessor.ToResetAfterKernel = append(
			commandProcessor.ToResetAfterKernel, dCache,
		)
	}

	for i := 0; i < 64; i++ {
		cacheBuilder.LowModuleFinder = lowModuleFinderForL1[i/16]
		kCache := cacheBuilder.BuildWriteAroundCache(
			fmt.Sprintf("%s.L1K_%02d", gpuName, i), 4, 16*mem.KB, 16)
		kCache.DirectoryLatency = 0
		kCache.Latency = 1
		kCache.SetNumBanks(1)
		connection.PlugIn(kCache.ToTop)
		connection.PlugIn(kCache.ToBottom)
		kCaches = append(kCaches, kCache)
		commandProcessor.ToResetAfterKernel = append(
			commandProcessor.ToResetAfterKernel, kCache,
		)

		cacheBuilder.LowModuleFinder = lowModuleFinderForL2
		iCache := cacheBuilder.BuildWriteAroundCache(
			fmt.Sprintf("%s.L1I_%02d", gpuName, i),
			4, 32*mem.KB, 16)
		iCache.DirectoryLatency = 0
		iCache.Latency = 0
		iCache.SetNumBanks(4)
		connection.PlugIn(iCache.ToTop)
		connection.PlugIn(iCache.ToBottom)
		iCaches = append(iCaches, iCache)
		commandProcessor.ToResetAfterKernel = append(
			commandProcessor.ToResetAfterKernel, iCache,
		)
	}

	for i := 0; i < 256; i++ {
		cuBuilder.CUName = fmt.Sprintf("%s.CU%02d", gpuName, i)
		cuBuilder.InstMem = iCaches[i/4].ToTop
		cuBuilder.ScalarMem = kCaches[i/4].ToTop
		//lowModuleFinderForCU := new(cache.SingleLowModuleFinder)
		//lowModuleFinderForCU.LowModule = dCaches[i].ToTop
		cuBuilder.VectorMemModules = lowModuleFinderForL1[i/64]
		cu := cuBuilder.Build()
		dispatcher.RegisterCU(cu.ToACE)

		connection.PlugIn(cu.ToACE)
	}

	dmaEngine := gcn3.NewDMAEngine(
		fmt.Sprintf("%s.DMA", gpuName), engine, lowModuleFinderForL2)
	commandProcessor.DMAEngine = dmaEngine.ToCommandProcessor

	rdmaEngines := configureRDMAEngineForLogicalGPU(
		engine, compressor, connection, interconnect, lowModuleFinderForL1,
		bufCapacity)
	configureL1ModuleFinders(lowModuleFinderForL1, l2Caches, rdmaEngines)

	connection.PlugIn(gpu.ToCommandProcessor)
	connection.PlugIn(gpu.ToDriver)
	connection.PlugIn(commandProcessor.ToDriver)
	connection.PlugIn(commandProcessor.ToDispatcher)
	connection.PlugIn(dispatcher.ToCommandProcessor)
	connection.PlugIn(dispatcher.ToCUs)
	connection.PlugIn(gpuMem.ToTop)
	connection.PlugIn(dmaEngine.ToCommandProcessor)
	connection.PlugIn(dmaEngine.ToMem)
	connection.PlugIn(gpuDriver.ToGPUs)

	gpu.Driver = gpuDriver.ToGPUs

	gpuDriver.RegisterGPU(gpu)

	return engine, gpuDriver, gpu, gpuMem, interconnect
}

func configureL1ModuleFinders(
	finders []*cache.InterleavedLowModuleFinder,
	l2s []*cache.WriteBackCache,
	rdmaEngines []*RDMAEngine,
) {
	for i, finder := range finders {
		for j, l2 := range l2s {
			if j/8 == i {
				finder.LowModules = append(finder.LowModules, l2.ToTop)
			} else {
				finder.LowModules = append(finder.LowModules,
					rdmaEngines[i].ToInside)
			}
		}
	}
}

func configureRDMAEngineForLogicalGPU(
	engine akita.Engine,
	compressor Compressor,
	intraConnect akita.Connection,
	interConnect akita.Connection,
	localFinder []*cache.InterleavedLowModuleFinder,
	bufCapacity int,
) []*RDMAEngine {
	rdmaEngines := make([]*RDMAEngine, 0, 4)
	remoteGPUFinder := cache.NewInterleavedLowModuleFinder(4096)
	for i := 0; i < 4; i++ {
		rdmaEngine := NewEngine(
			fmt.Sprintf("GPU1.RDMAEngine%d", i),
			engine,
			compressor,
			localFinder[i],
			remoteGPUFinder)
		rdmaEngine.ToInside.BufCapacity = bufCapacity
		rdmaEngine.ToOutside.BufCapacity = bufCapacity
		intraConnect.PlugIn(rdmaEngine.ToInside)
		interConnect.PlugIn(rdmaEngine.ToOutside)
		rdmaEngines = append(rdmaEngines, rdmaEngine)
		for j := 0; j < 8; j++ {
			remoteGPUFinder.LowModules = append(remoteGPUFinder.LowModules,
				rdmaEngine.ToOutside)
		}
	}
	return rdmaEngines
}

func rewireACEandCUs(gpus []*gcn3.GPU) {
	headGPU := gpus[0]
	headACE := headGPU.Dispatchers[0].(*gcn3.Dispatcher)
	// headACE.CUs = make([]*akita.Port, 0)
	connection := headGPU.ToCommandProcessor.Conn

	for i, gpu := range gpus {
		if i == 0 {
			continue
		}

		for _, cu := range gpu.CUs {
			cu := cu.(*timing.ComputeUnit)
			headACE.RegisterCU(cu.ToACE)
			connection.PlugIn(cu.ToACE)
		}
	}
}
