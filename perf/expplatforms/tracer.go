package expplatforms

import (
	"fmt"
	"io"
	"reflect"
	"strings"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
)

type Tracer struct {
	sb     strings.Builder
	writer io.Writer
}

func NewTracer(writer io.Writer) *Tracer {
	t := new(Tracer)
	//t.writer = gzip.NewWriter(writer)
	t.writer = writer
	return t
}

func (t *Tracer) Type() reflect.Type {
	return reflect.TypeOf((akita.Req)(nil))
}

func (t *Tracer) Pos() akita.HookPos {
	return akita.ConnStartSendHookPos
}

func (t *Tracer) Func(
	item interface{},
	domain akita.Hookable,
	info interface{},
) {
	switch req := item.(type) {
	case *mem.ReadReq:
		s := fmt.Sprintf("R %s -> %s addr: 0x%x, size: %d\n",
			req.Src().Comp.Name(), req.Dst().Comp.Name(),
			req.Address, req.MemByteSize)
		t.writer.Write([]byte(s))

	case *mem.WriteReq:
		fmt.Fprintf(&t.sb, "W %s -> %s addr: 0x%x, data: ",
			req.Src().Comp.Name(), req.Dst().Comp.Name(),
			req.Address)
		for _, b := range req.Data {
			fmt.Fprintf(&t.sb, "%02x ", b)
		}
		fmt.Fprintf(&t.sb, "\n")
		t.writer.Write([]byte(t.sb.String()))
		t.sb.Reset()

	case *mem.DataReadyRsp:
		fmt.Fprintf(&t.sb, "DR %s -> %s data: ",
			req.Src().Comp.Name(), req.Dst().Comp.Name())
		for _, b := range req.Data {
			fmt.Fprintf(&t.sb, "%02x ", b)
		}
		fmt.Fprintf(&t.sb, "\n")
		t.writer.Write([]byte(t.sb.String()))
		t.sb.Reset()

	case *mem.DoneRsp:
		s := fmt.Sprintf("WD %s -> %s\n",
			req.Src().Comp.Name(), req.Dst().Comp.Name())
		t.writer.Write([]byte(s))
	}
}
