from tqdm import tqdm
import subprocess
from multiprocessing.pool import ThreadPool

pbar = None

def run(benchmark, input_scheme, input_size, comp, freq, buf, trace):
    out_fp = open(benchmark + '_' + comp + '_' + input_scheme + '_' +
              str(freq) + '_' + str(buf) + '_stdout.out', 'w')
    err_fp = open(benchmark + '_' + comp + '_' + input_scheme + '_' +
              str(freq) + '_' + str(buf) + '_stderr.out', 'w')

    cmd = './perf -benchmark={0} -input={1} -compression={2} -cu-freq={3} -net-buf={4} -x={5}'.format(
        benchmark, input_scheme, comp, freq*1048576, buf, input_size)
    if trace:
        cmd += ' -trace'
    # print(cmd)

    p = subprocess.Popen(cmd, shell=True, stdout=out_fp, stderr=err_fp)
    p.wait()
    pbar.update(1)


def main():
    global pbar
    benchmarks = ['fir', 'aes', 'gd', 'sc', 'mt', 'bs', 'km']
    inputSizes = [1048576, 1048576, 1048576, 2046, 2048, 65536, 32768]
    input_schemes = ['sparse_wide', 'sparse_narrow', 'sparse_seq']
    comp_algs = ['none', 'cpack', 'bdi', 'fpc', 'adaptive', 'adaptive2', 'adaptive3']
    cu_freqs = [1000]
    net_bufs = [1, 8, 64, 512]

    pbar = tqdm(total=len(benchmarks) * len(input_schemes) * len(comp_algs) * len(cu_freqs) * len(net_bufs))

    tp = ThreadPool()
    for idx, b in enumerate(benchmarks):
        for input_scheme in input_schemes:
            trace = True
            for a in comp_algs:
                for f in cu_freqs:
                    for n in net_bufs:
                        tp.apply_async(run, (b, input_scheme, inputSizes[idx], a, f, n, trace, ))
                        trace = False
    tp.close()
    tp.join()



if __name__ == '__main__':
    main()
