package gd

import (
	"log"
	"math/rand"
	"strings"

	"gitlab.com/akita/gcn3/driver"
	"gitlab.com/akita/gcn3/insts"
	"gitlab.com/akita/gcn3/kernels"
)

type GDKernelArgs struct {
	Model               driver.GPUPtr
	Gradient0           driver.GPUPtr
	Gradient1           driver.GPUPtr
	Gradient2           driver.GPUPtr
	Gradient3           driver.GPUPtr
	NumCopies           uint32
	Padding             uint32
	HiddenGlobalOffsetX int64
	HiddenGlobalOffsetY int64
	HiddenGlobalOffsetZ int64
}

type Benchmark struct {
	driver *driver.Driver

	hsaco *insts.HsaCo

	InputScheme  string
	NumParam     int
	NumCopy      int
	ModelData    []float32
	gradientData [][]float32
	gModel       driver.GPUPtr
	gGradient    []driver.GPUPtr
}

func NewBenchmark(driver *driver.Driver) *Benchmark {
	b := new(Benchmark)

	b.driver = driver

	b.loadKernels()

	return b
}

func (b *Benchmark) loadKernels() {
	hsacoBytes, err := Asset("kernels.hsaco")
	if err != nil {
		log.Panic(err)
	}
	b.hsaco = kernels.LoadProgramFromMemory(
		hsacoBytes, "GD")
}

func (b *Benchmark) Run() {
	b.initMem()
	b.exec()
}

func (b *Benchmark) initMem() {
	b.gModel = b.driver.AllocateMemory(uint64(b.NumParam * 4))
	b.gGradient = make([]driver.GPUPtr, b.NumCopy)
	for i := 0; i < b.NumCopy; i++ {
		b.gGradient[i] = b.driver.AllocateMemory(uint64(b.NumParam * 4))
	}

	modelData := make([]float32, b.NumParam)
	for i := 0; i < b.NumParam; i++ {
		if strings.Contains(b.InputScheme, "wide") {
			modelData[i] = rand.Float32()
		} else if strings.Contains(b.InputScheme, "seq") {
			modelData[i] = float32(i)
		} else {
			modelData[i] = float32(rand.Uint32() % 100)
		}
	}
	for i := 0; i < b.NumParam; i++ {
		if strings.Contains(b.InputScheme, "sparse") {
			dice := rand.Float64()
			if dice < 0.9 {
				modelData[i] = 0
			}
		}
	}

	b.gradientData = make([][]float32, b.NumCopy)
	for i := 0; i < b.NumCopy; i++ {
		b.gradientData[i] = make([]float32, b.NumParam)
		for j := 0; j < b.NumParam; j++ {
			if strings.Contains(b.InputScheme, "wide") {
				b.gradientData[i][j] = rand.Float32()
			} else {
				b.gradientData[i][j] = float32(rand.Uint32() % 100)
			}
		}
		for j := 0; j < b.NumParam; j++ {
			if strings.Contains(b.InputScheme, "sparse") {
				dice := rand.Float64()
				if dice < 0.9 {
					b.gradientData[i][i] = 0
				}
			}
		}
	}

	b.driver.MemoryCopyHostToDevice(b.gModel, modelData)
	for i := 0; i < b.NumCopy; i++ {
		b.driver.MemoryCopyHostToDevice(b.gGradient[i], b.gradientData[i])
	}
}

func (b *Benchmark) exec() {
	kernArg := GDKernelArgs{
		b.gModel,
		b.gGradient[0],
		b.gGradient[1],
		b.gGradient[2],
		b.gGradient[3],
		uint32(b.NumCopy),
		0,
		0, 0, 0,
	}

	b.driver.LaunchKernel(
		b.hsaco,
		[3]uint32{uint32(b.NumParam), 1, 1},
		[3]uint16{64, 1, 1},
		&kernArg,
	)
}

func (b *Benchmark) Verify() {
	log.Printf("Passed!\n")
}
