package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/gcn3/driver"
	"gitlab.com/akita/noc"

	"gitlab.com/syifan/ipdps2019/perf/benchmarks/aes"
	"gitlab.com/syifan/ipdps2019/perf/benchmarks/bitonicsort"
	"gitlab.com/syifan/ipdps2019/perf/benchmarks/fir"
	"gitlab.com/syifan/ipdps2019/perf/benchmarks/gd"
	"gitlab.com/syifan/ipdps2019/perf/benchmarks/kmeans"
	"gitlab.com/syifan/ipdps2019/perf/benchmarks/matrixtranspose"
	"gitlab.com/syifan/ipdps2019/perf/benchmarks/simpleconvolution"
	"gitlab.com/syifan/ipdps2019/perf/compression"
	"gitlab.com/syifan/ipdps2019/perf/expplatforms"
)

var benchmarkFlag = flag.String("benchmark", "", "")
var inputFlag = flag.String("input", "wide-dense", "")
var traceFlag = flag.Bool("trace", false, "")
var compFlag = flag.String("compression", "none", "")
var cuFreqFlag = flag.Float64("cu-freq", 1048576000, "")
var netBufFlag = flag.Int("net-buf", 1, "")
var sizeFlag = flag.Int("x", 1048576, "")
var verifyFlag = flag.Bool("verify", false, "")

// A Benchmark is the program to test
type Benchmark interface {
	Run()
	Verify()
}

// An Experiment runs a benchmark with a specific configuration
type Experiment struct {
	engine            akita.Engine
	gpuDriver         *driver.Driver
	interConnect      akita.Connection
	kernelTimeCounter *driver.KernelTimeCounter
	trafficCounter    *noc.TrafficCounter
	compressor        expplatforms.Compressor
	benchmark         Benchmark

	BenchmarkName     string
	CompAlg           string
	InputScheme       string
	InputSize         int
	CUFreq            akita.Freq
	NetworkBufferSize int
	KernelTime        akita.VTimeInSec
}

func newExperiment(
	benchmarkName,
	compAlg string,
	inputScheme string,
	cuFreq akita.Freq,
	networkBufferSize int,
) *Experiment {
	e := new(Experiment)
	e.BenchmarkName = benchmarkName
	e.CompAlg = compAlg
	e.InputScheme = inputScheme
	e.CUFreq = cuFreq
	e.NetworkBufferSize = networkBufferSize
	e.InputSize = *sizeFlag

	switch compAlg {
	case "none":
		e.compressor = new(compression.NoneCompressor)
	case "cpack":
		e.compressor = new(compression.CPACK)
	case "fpc":
		e.compressor = new(compression.FPC)
	case "bdi":
		e.compressor = new(compression.BDI)
	case "adaptive":
		e.compressor = compression.NewAdaptive()
	case "adaptive2":
		e.compressor = compression.NewAdaptive2()
	case "adaptive3":
		e.compressor = compression.NewAdaptive3()
	default:
		panic("unknown compression algorithm\n")
	}

	e.engine, e.gpuDriver, _, _, e.interConnect =
		expplatforms.BuildLogicalGPU(e.compressor, cuFreq, networkBufferSize)

	if *traceFlag {
		filename := fmt.Sprintf("%s_%s_%d.log",
			benchmarkName, inputScheme, e.InputSize)
		interconnectLogFile, _ := os.Create(filename)
		tracer := expplatforms.NewTracer(interconnectLogFile)
		e.interConnect.AcceptHook(tracer)
	}

	e.kernelTimeCounter = new(driver.KernelTimeCounter)
	e.gpuDriver.AcceptHook(e.kernelTimeCounter)

	e.trafficCounter = new(noc.TrafficCounter)
	e.interConnect.AcceptHook(e.trafficCounter)

	return e
}

// Run runs all benchmarks
func (e *Experiment) Run() {
	e.benchmark.Run()
	if *verifyFlag {
		e.benchmark.Verify()
	}
	fmt.Printf("%s,%s,%s,%d,%f,%d,%.12f,%d,%d,%d,%d,%d\n",
		e.BenchmarkName, e.CompAlg, e.InputScheme,
		e.InputSize, e.CUFreq, e.NetworkBufferSize,
		e.kernelTimeCounter.TotalTime,
		e.trafficCounter.TotalData,
		e.compressor.NoneCount(),
		e.compressor.BDICount(),
		e.compressor.FPCCount(),
		e.compressor.CPACKCount(),
	)
}

func configExperiment() *Experiment {
	exp := newExperiment(
		*benchmarkFlag,
		*compFlag,
		*inputFlag,
		akita.Freq(*cuFreqFlag),
		*netBufFlag)

	switch *benchmarkFlag {
	case "fir":
		b := fir.NewBenchmark(exp.gpuDriver)
		b.Length = *sizeFlag
		b.InputScheme = *inputFlag
		exp.benchmark = b
	case "aes":
		b := aes.NewBenchmark(exp.gpuDriver)
		b.Length = *sizeFlag
		b.InputScheme = *inputFlag
		exp.benchmark = b
	case "km":
		b := kmeans.NewBenchmark(exp.gpuDriver)
		b.NumClusters = 5
		b.NumFeatures = 32
		b.NumPoints = *sizeFlag
		b.MaxIter = 1
		b.InputScheme = *inputFlag
		exp.benchmark = b
	case "mt":
		b := matrixtranspose.NewBenchmark(exp.gpuDriver)
		b.Width = *sizeFlag
		b.InputScheme = *inputFlag
		exp.benchmark = b
	case "bs":
		b := bitonicsort.NewBenchmark(exp.gpuDriver)
		b.Length = *sizeFlag
		b.InputScheme = *inputFlag
		exp.benchmark = b
	case "gd":
		b := gd.NewBenchmark(exp.gpuDriver)
		b.NumParam = *sizeFlag
		b.NumCopy = 4
		b.InputScheme = *inputFlag
		exp.benchmark = b
	case "sc":
		b := simpleconvolution.NewBenchmark(exp.gpuDriver)
		b.Width = uint32(*sizeFlag)
		b.Height = uint32(*sizeFlag)
		b.SetMaskSize(3)
		b.InputScheme = *inputFlag
		exp.benchmark = b

	default:
		panic("unknown benchmark name")
	}
	return exp
}

func main() {
	flag.Parse()
	b := configExperiment()
	b.Run()
}
