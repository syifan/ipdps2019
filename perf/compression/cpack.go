package compression

import (
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
)

type CPACK struct {
	//compData   map[uint64][]byte
	decompData map[uint64][]byte
	compSize   map[uint64]uint32
	cpackCount int
}

func (c *CPACK) NoneCount() int {
	return 0
}

func (c *CPACK) BDICount() int {
	return 0
}

func (c *CPACK) FPCCount() int {
	return 0
}

func (c *CPACK) CPACKCount() int {
	return c.cpackCount
}

func (c *CPACK) CompressCycles(now akita.VTimeInSec, req akita.Req) int {
	return 16
}

func (c *CPACK) DecompressCycles(now akita.VTimeInSec, req akita.Req) int {
	switch req := req.(type) {
	case *mem.DataReadyRsp:
		if req.CompressedByteSize == req.ByteSize() {
			return 0
		}
		return 9
	case *mem.WriteReq:
		if req.CompressedByteSize == req.ByteSize() {
			return 0
		}
		return 9
	default:
		panic("unknown type")
	}
	return 9
}

func (c *CPACK) Compress(addr uint64, data []byte) uint32 {
	c.cpackCount++
	const blockSize = 64
	c.compSize = make(map[uint64]uint32)
	c.decompData = make(map[uint64][]byte)

	var compSize uint32 = 0
	var dictSize int = 0
	dict := [64]byte{0}
	// zero blcok detector
	if isZero(data) {
		return 2
	}

	for i := 0; i < 16; i++ {
		dictHit := false

		value := convertInt32([]byte{data[4*i], data[4*i+1], data[4*i+2], data[4*i+3]})
		// zero word detector
		if value == 0 {
			compSize += 2
			continue
		} else {
			//Narrow word with one byte significance
			if value & ^(int32(0x000000ff)) == 0 {
				compSize += 12
				continue
			}
		}
		for j := 0; j < dictSize; j++ {
			dictVal := convertInt32(dict[4*j : 4*j+4])

			if value == dictVal {
				//Word is matched
				compSize += 8
				dictHit = true
				break
			}
			//Three bytes are matched
			if value & ^(int32(0x000000ff)) == dictVal & ^(int32(0x000000ff)) {
				compSize += 16
				dictHit = true
				break
			}
			//Halfword is matched
			if value & ^(int32(0x0000ffff)) == dictVal & ^(int32(0x0000ffff)) {
				compSize += 24
				dictHit = true
				break
			}
		}

		if dictHit == true {
			continue
		} else {
			//New word (insert into the dictionary)
			compSize += 34
			copy(dict[4*dictSize:4*dictSize+4], data[4*i:4*i+4])
			dictSize++
		}
	}
	if compSize > 512 {
		compSize = 512
	}
	return compSize
}
