package compression

import (
	"log"
	"os"

	"gitlab.com/akita/akita"
)

type Adaptive struct {
	bdi   *BDI
	cpack *CPACK
	fpc   *FPC

	selected           string
	runningPhaseLeft   int
	runningPhaseLength int

	noneVote          int
	bdiVote           int
	cpackVote         int
	fpcVote           int
	totalVoteRequired int

	noneCount, bdiCount, fpcCount, cpackCount int

	logger *log.Logger
}

func (c *Adaptive) NoneCount() int {
	return c.noneCount
}

func (c *Adaptive) BDICount() int {
	return c.bdiCount
}

func (c *Adaptive) FPCCount() int {
	return c.fpcCount
}

func (c *Adaptive) CPACKCount() int {
	return c.cpackCount
}

func NewAdaptive() *Adaptive {
	c := new(Adaptive)
	c.bdi = new(BDI)
	c.cpack = new(CPACK)
	c.fpc = new(FPC)

	c.totalVoteRequired = 7
	c.runningPhaseLength = 300

	c.logger = log.New(os.Stderr, "", 0)

	return c
}

func (c *Adaptive) Compress(addr uint64, data []byte) uint32 {
	var size uint32
	if c.selected == "" {
		noneSize := uint32(len(data) * 8)
		bdiSize := c.bdi.Compress(addr, data)
		cpackSize := c.cpack.Compress(addr, data)
		fpcSize := c.fpc.Compress(addr, data)
		if noneSize <= bdiSize && noneSize <= fpcSize && noneSize <= cpackSize {
			c.noneVote++
			size = noneSize
		} else if bdiSize <= cpackSize && bdiSize <= fpcSize {
			c.bdiVote++
			size = bdiSize
		} else if fpcSize <= cpackSize {
			c.fpcVote++
			size = fpcSize
		} else {
			c.cpackVote++
			size = cpackSize
		}

		c.noneCount++
		c.fpcCount++
		c.bdiCount++
		c.cpackCount++

		if c.noneVote+c.bdiVote+c.fpcVote+c.cpackVote == c.totalVoteRequired {
			if c.noneVote >= c.bdiVote && c.noneVote >= c.fpcVote && c.noneVote >= c.cpackVote {
				c.selected = "none"
			} else if c.bdiVote >= c.fpcVote && c.bdiVote >= c.cpackVote {
				c.selected = "bdi"
			} else if c.fpcVote >= c.cpackVote {
				c.selected = "fpc"
			} else {
				c.selected = "cpack"
			}
			c.noneVote = 0
			c.bdiVote = 0
			c.fpcVote = 0
			c.cpackVote = 0
			c.runningPhaseLeft = c.runningPhaseLength
			c.logger.Printf("comp select: %s\n", c.selected)
		}
	} else {
		switch c.selected {
		case "none":
			size = uint32(len(data) * 8)
			c.noneCount++
		case "bdi":
			size = c.bdi.Compress(addr, data)
			c.bdiCount++
		case "fpc":
			size = c.fpc.Compress(addr, data)
			c.fpcCount++
		case "cpack":
			size = c.cpack.Compress(addr, data)
			c.cpackCount++
		}
		c.runningPhaseLeft--
		if c.runningPhaseLeft == 0 {
			c.selected = ""
		}
	}

	return size
}

func (c *Adaptive) CompressCycles(now akita.VTimeInSec, req akita.Req) int {
	switch c.selected {
	case "none":
		return 0
	case "", "cpack":
		return c.cpack.CompressCycles(now, req)
	case "fpc":
		return c.fpc.CompressCycles(now, req)
	case "bdi":
		return c.bdi.CompressCycles(now, req)
	default:
		panic(c.selected)
	}
}

func (c *Adaptive) DecompressCycles(now akita.VTimeInSec, req akita.Req) int {
	switch c.selected {
	case "none":
		return 0
	case "", "cpack":
		return c.cpack.DecompressCycles(now, req)
	case "fpc":
		return c.fpc.DecompressCycles(now, req)
	case "bdi":
		return c.bdi.DecompressCycles(now, req)
	default:
		panic(c.selected)
	}
}
