package compression

import (
	"math"
)

func entropy(data []byte) float64 {
	byteMap := make(map[byte]byte)
	var entropy float64 = 0.0
	for _, value := range data {
		if count, ok := byteMap[value]; ok {
			count++
			byteMap[value] = count
		} else {
			byteMap[value] = 1
		}
	}

	for _, val := range byteMap {
		//fmt.Printf("key:%02x val:%d\t", key, val)
		p := float64(val) / 64.0
		entropy += p * math.Log(p) / math.Log(float64(64.0))
	}
	if entropy == 0 {
		return entropy
	} else {
		return -entropy
	}

}
