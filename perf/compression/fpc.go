package compression

import (
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem"
)

type FPC struct {
	//compData   map[uint64][]byte
	decompData map[uint64][]byte
	compSize   map[uint64]uint32
	fpcCount   int
}

func (c *FPC) NoneCount() int {
	return 0
}

func (c *FPC) BDICount() int {
	return 0
}

func (c *FPC) FPCCount() int {
	return c.fpcCount
}

func (c *FPC) CPACKCount() int {
	return 0
}

func (c *FPC) CompressCycles(now akita.VTimeInSec, req akita.Req) int {
	return 3
}

func (c *FPC) DecompressCycles(now akita.VTimeInSec, req akita.Req) int {
	switch req := req.(type) {
	case *mem.DataReadyRsp:
		if req.CompressedByteSize == req.ByteSize() {
			return 0
		}
		return 5
	case *mem.WriteReq:
		if req.CompressedByteSize == req.ByteSize() {
			return 0
		}
		return 5
	default:
		panic("unknown type")
	}
	return 5
}

func abs(data int32) uint32 {
	temp := data >> 31
	return uint32((data ^ temp) - temp)
}

/*
func convertInt32(data []byte) int32 {
	var output int32
	buf := bytes.NewBuffer(data)
	err := binary.Read(buf, binary.LittleEndian, &output)
	if err != nil {
		log.Printf("Binary read failes in FPC", err)
	}
	return output
}

func readPack(pack []byte) uint32 {
	val := convertInt32(pack)
	return abs(val)
}
*/
// not the actual implementation of decompressor
func (c *FPC) Decompress(addr uint64) []byte {
	return c.decompData[addr]
}

// implements the functionality of FPC and updates the compression size
// decompressed form of the data is not saved -- can be added if needed in the future
// output is the size of data after compression in **bit**
func (c *FPC) Compress(addr uint64, data []byte) uint32 {
	c.fpcCount++
	c.compSize = make(map[uint64]uint32)
	c.decompData = make(map[uint64][]byte)
	c.decompData[addr] = data
	wordNum := len(data) / 4
	var size uint32
	size = 0
	if isZero(data) {
		return 3
	}
	for i := 0; i < wordNum; i++ {
		value := readPack32([]byte{data[4*i], data[4*i+1], data[4*i+2], data[4*i+3]})
		// 000
		if value == 0 {
			continue
		}
		// 001
		if value <= 0xF {
			size += 4
			continue
		}
		// 010
		if value <= 0xFF {
			size += 8
			continue
		}
		// 011
		if value <= 0xFFFF {
			size += 16
			continue
		}
		// 100  halfword padded with a zero halfword
		if (value & 0xFFFF) == 0 {
			size += 16
			continue
		}
		// 101 Two halfwords, each a byte sign-extended
		if ((value & 0xFFFF) <= 0xFF) && ((value>>16)&0xFFFF) <= 0xFF {
			size += 16
			continue
		}
		// 110 word consisting of repeated bytes
		byte0 := value & 0xFF
		byte1 := (value >> 8) & 0xFF
		byte2 := (value >> 16) & 0xFF
		byte3 := (value >> 24) & 0xFF
		if byte0 == byte1 && byte0 == byte2 && byte0 == byte3 {
			size += 8
			continue
		}
		// 111 Uncompressed

		size += 32
	}
	size += 48 // metadata for FPC
	if size > 512 {
		size = 512
	}
	c.compSize[addr] = size
	return size
}
