package compression

import "gitlab.com/akita/akita"

// A NoneCompressor is a compressor that does not perform any compression
type NoneCompressor struct {
	nonCount int
}

func (c *NoneCompressor) NoneCount() int {
	return c.nonCount
}

func (NoneCompressor) BDICount() int {
	return 0
}

func (NoneCompressor) FPCCount() int {
	return 0
}

func (NoneCompressor) CPACKCount() int {
	return 0
}

func (c *NoneCompressor) Compress(addr uint64, data []byte) uint32 {
	c.nonCount++
	return uint32(len(data) * 8)
}

func (NoneCompressor) CompressCycles(now akita.VTimeInSec, req akita.Req) int {
	return 0
}

func (NoneCompressor) DecompressCycles(now akita.VTimeInSec, req akita.Req) int {
	return 0
}
