package compression

import (
	"bytes"
	"encoding/binary"
	"log"

	"gitlab.com/akita/mem"

	"gitlab.com/akita/akita"
)

type BDI struct {
	//compData   map[uint64][]byte
	decompData map[uint64][]byte
	compSize   map[uint64]uint32
	bdiCount   int
}

func (c *BDI) NoneCount() int {
	return 0
}

func (c *BDI) BDICount() int {
	return c.bdiCount
}

func (c *BDI) FPCCount() int {
	return 0
}

func (c *BDI) CPACKCount() int {
	return 0
}

func (c *BDI) CompressCycles(now akita.VTimeInSec, req akita.Req) int {
	return 2
}

func (c *BDI) DecompressCycles(now akita.VTimeInSec, req akita.Req) int {
	switch req := req.(type) {
	case *mem.DataReadyRsp:
		if req.CompressedByteSize == req.ByteSize() {
			return 0
		}
		return 1
	case *mem.WriteReq:
		if req.CompressedByteSize == req.ByteSize() {
			return 0
		}
		return 1
	default:
		panic("unknown type")
	}
	return 1
}

func min(a, b uint32) uint32 {
	if a <= b {
		return a
	}
	return b
}

func isZero(data []byte) bool {
	yes := true
	for _, val := range data {
		if val != 0x00 {
			yes = false
			break
		}
	}
	return yes
}

func isSameValue(data []byte, size int) bool {
	yes := true
	numChunk := 64 / size

	if size == 4 {
		value := readPack32(data[0:size])
		for i := 1; i < numChunk; i++ {
			if value != readPack32(data[i*size:i*size+size]) {
				yes = false
				break
			}
		}
	}

	if size == 8 {
		value := readPack64(data[0:size])
		for i := 1; i < numChunk; i++ {
			if value != readPack64(data[i*size:i*size+size]) {
				yes = false
				break
			}
		}
	}

	return yes
}

func multBase(data []byte, baseSize uint32, deltaSize uint32) uint32 { // size = 8, 16, 32    (8, 4, 2)
	var limit uint64 = 0
	var compSize uint32 = 512

	numChunk := 64 / baseSize
	switch deltaSize {
	case 1:
		limit = 0xFF
	case 2:
		limit = 0xFFFF
	case 4:
		limit = 0xFFFFFFFF
	default:
		log.Panicf("error in baseSize value!")
	}

	var i uint32
	if baseSize == 2 {
		var base uint16
		base = readPack16(data[0:2])
		for i = 1; i < numChunk; i++ {
			value := readPack16(data[i*baseSize : i*baseSize+baseSize])
			if abs16(int16(base-value)) > uint16(limit) {
				return compSize
			}
		}
		compSize = deltaSize*numChunk + baseSize + numChunk/8
		//fmt.Printf("baseVal %d base %d, delta %d, compSize %d\n", base, baseSize, deltaSize, compSize)
	}

	if baseSize == 4 {
		var base uint32
		base = readPack32(data[0:4])
		for i = 1; i < numChunk; i++ {
			value := readPack32(data[i*baseSize : i*baseSize+baseSize])
			if abs32(int32(base-value)) > uint32(limit) {
				return compSize
			}
		}
		compSize = deltaSize*numChunk + baseSize + numChunk/8
		//fmt.Printf("baseVal %d base %d, delta %d, compSize %d\n", base, baseSize, deltaSize, compSize)

	}

	if baseSize == 8 {
		var base uint64
		base = readPack64(data[0:8])
		for i = 1; i < numChunk; i++ {
			value := readPack64(data[i*baseSize : i*baseSize+baseSize])
			if abs64(int64(base-value)) > limit {
				return compSize
			}
		}
		compSize = deltaSize*numChunk + baseSize + numChunk/8
		//fmt.Printf("baseVal %d base %d, delta %d, compSize %d\n", base, baseSize, deltaSize, compSize)

	}

	//fmt.Printf("%d-bases bsize = %d DeltaSize = %d CompCount = %d CompSize = %d\n", BASES, bsize, blimit, compCount, compSize)
	return compSize
}

func abs64(data int64) uint64 {
	temp := data >> 63
	return uint64((data ^ temp) - temp)
}

func abs32(data int32) uint32 {
	temp := data >> 31
	return uint32((data ^ temp) - temp)
}

func abs16(data int16) uint16 {
	temp := data >> 15
	return uint16((data ^ temp) - temp)
}

func convertInt64(data []byte) int64 {
	var output int64
	buf := bytes.NewBuffer(data)
	err := binary.Read(buf, binary.LittleEndian, &output)
	if err != nil {
		log.Printf("Binary read failes in convertInt64")
	}
	return output
}

func convertInt32(data []byte) int32 {
	var output int32
	buf := bytes.NewBuffer(data)
	err := binary.Read(buf, binary.LittleEndian, &output)
	if err != nil {
		log.Printf("Binary read failes in convertInt32")
	}
	return output
}

func convertInt16(data []byte) int16 {
	var output int16
	buf := bytes.NewBuffer(data)
	err := binary.Read(buf, binary.LittleEndian, &output)
	if err != nil {
		log.Printf("Binary read failes in convertInt16")
	}
	return output
}

func readPack64(pack []byte) uint64 {
	val := convertInt64(pack)
	return abs64(val)
}
func readPack32(pack []byte) uint32 {
	val := convertInt32(pack)
	return abs32(val)
}
func readPack16(pack []byte) uint16 {
	val := convertInt16(pack)
	return abs16(val)
}

// not an actual implementation of decompressor
func (c *BDI) Decompress(addr uint64) []byte {
	return c.decompData[addr]
}

func (c *BDI) Compress(addr uint64, data []byte) uint32 {
	c.bdiCount++
	const blockSize = 64
	c.compSize = make(map[uint64]uint32)
	c.decompData = make(map[uint64][]byte)
	c.decompData[addr] = data
	var bestSize uint32 = 64
	var currentSize uint32 = 64
	if isZero(data) {
		bestSize = 1
	}
	if isSameValue(data, 8) {
		currentSize = 8
	}

	bestSize = min(bestSize, currentSize)
	// 8 byte here
	currentSize = multBase(data, 8, 1)
	bestSize = min(bestSize, currentSize)

	currentSize = multBase(data, 8, 2)
	bestSize = min(bestSize, currentSize)

	currentSize = multBase(data, 8, 4)
	bestSize = min(bestSize, currentSize)

	// 4 bytes here --- can improve compressibility slightly but it is not in the paper
	//if isSameValue(data, 4) {
	//	currentSize = 4
	//}

	bestSize = min(bestSize, currentSize)

	currentSize = multBase(data, 4, 1)
	bestSize = min(bestSize, currentSize)

	currentSize = multBase(data, 4, 2)
	bestSize = min(bestSize, currentSize)

	currentSize = multBase(data, 2, 1)
	bestSize = min(bestSize, currentSize)

	bestSize = bestSize*8 + 4
	if bestSize > 512 {
		bestSize = 512
	}
	return bestSize
}
