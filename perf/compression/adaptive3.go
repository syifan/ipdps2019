package compression

import (
	"log"
	"os"

	"gitlab.com/akita/akita"
)

type Adaptive3 struct {
	bdi   *BDI
	cpack *CPACK
	fpc   *FPC

	selected           string
	runningPhaseLeft   int
	runningPhaseLength int

	noneVote          int
	bdiVote           int
	cpackVote         int
	fpcVote           int
	totalVoteRequired int

	noneCount, bdiCount, fpcCount, cpackCount int

	logger *log.Logger
}

func (c *Adaptive3) NoneCount() int {
	return c.noneCount
}

func (c *Adaptive3) BDICount() int {
	return c.bdiCount
}

func (c *Adaptive3) FPCCount() int {
	return c.fpcCount
}

func (c *Adaptive3) CPACKCount() int {
	return c.cpackCount
}

func NewAdaptive3() *Adaptive3 {
	c := new(Adaptive3)
	c.bdi = new(BDI)
	c.cpack = new(CPACK)
	c.fpc = new(FPC)

	c.totalVoteRequired = 7
	c.runningPhaseLength = 300

	c.logger = log.New(os.Stderr, "", 0)

	return c
}

func (c *Adaptive3) Compress(addr uint64, data []byte) uint32 {
	var size uint32
	if c.selected == "" {
		noneSize := uint32(len(data) * 8)
		bdiSize := c.bdi.Compress(addr, data)
		cpackSize := c.cpack.Compress(addr, data)
		fpcSize := c.fpc.Compress(addr, data)

		noneValue := noneSize + 6*0
		bdiValue := bdiSize + 6*3
		fpcValue := fpcSize + 6*8
		cpackValue := cpackSize + 6*27

		if noneValue <= bdiValue && noneValue <= fpcValue && noneValue <= cpackValue {
			c.noneVote++
			size = noneSize
		} else if bdiValue <= cpackValue && bdiValue <= fpcValue {
			c.bdiVote++
			size = bdiSize
		} else if fpcValue <= cpackValue {
			c.fpcVote++
			size = fpcSize
		} else {
			c.cpackVote++
			size = cpackSize
		}

		c.noneCount++
		c.cpackCount++
		c.bdiCount++
		c.fpcCount++

		if c.noneVote+c.bdiVote+c.fpcVote+c.cpackVote == c.totalVoteRequired {
			if c.noneVote >= c.bdiVote && c.noneVote >= c.fpcVote && c.noneVote >= c.cpackVote {
				c.selected = "none"
			} else if c.bdiVote >= c.fpcVote && c.bdiVote >= c.cpackVote {
				c.selected = "bdi"
			} else if c.fpcVote >= c.cpackVote {
				c.selected = "fpc"
			} else {
				c.selected = "cpack"
			}
			c.noneVote = 0
			c.bdiVote = 0
			c.fpcVote = 0
			c.cpackVote = 0
			c.runningPhaseLeft = c.runningPhaseLength
			c.logger.Printf("comp select: %s\n", c.selected)
		}
	} else {
		switch c.selected {
		case "none":
			size = uint32(len(data) * 8)
			c.noneCount++
		case "bdi":
			size = c.bdi.Compress(addr, data)
			c.bdiCount++
		case "fpc":
			size = c.fpc.Compress(addr, data)
			c.fpcCount++
		case "cpack":
			size = c.cpack.Compress(addr, data)
			c.cpackCount++
		}
		c.runningPhaseLeft--
		if c.runningPhaseLeft == 0 {
			c.selected = ""
		}
	}

	return size
}

func (c *Adaptive3) CompressCycles(now akita.VTimeInSec, req akita.Req) int {
	switch c.selected {
	case "none":
		return 0
	case "", "cpack":
		return c.cpack.CompressCycles(now, req)
	case "fpc":
		return c.fpc.CompressCycles(now, req)
	case "bdi":
		return c.bdi.CompressCycles(now, req)
	default:
		panic(c.selected)
	}
}

func (c *Adaptive3) DecompressCycles(now akita.VTimeInSec, req akita.Req) int {
	switch c.selected {
	case "none":
		return 0
	case "", "cpack":
		return c.cpack.DecompressCycles(now, req)
	case "fpc":
		return c.fpc.DecompressCycles(now, req)
	case "bdi":
		return c.bdi.DecompressCycles(now, req)
	default:
		panic(c.selected)
	}
}
